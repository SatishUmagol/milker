<?php

class DB_Connection
{   
    public $conn;

    function __construct() {  
        require_once ('config.php');  

        $this->conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_DATABSE);  
        if(!$this->conn)// testing the connection  
        {  
            die ("Cannot connect to the database");  
        }  
        return $this->conn;  
    }  
    
    public function Close(){  
        mysql_close();  
    }   
}
?>