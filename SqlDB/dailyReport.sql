create table DailyReport
(
    Report_Id int(10) NOT NULL AUTO_INCREMENT,
    Milkman_Id int(10) NOT NULL,
    Customer_Id int(10) NOT NULL,
    Today_Date  text NOT NULL,
    Today_Milk text NOT NULL,
    Today_Bill text NOT NULL,
    Foreign key (Milkman_Id) REFERENCES Milkman (Milkman_Id) on DELETE cascade on UPDATE cascade,
    Foreign key (Customer_Id) REFERENCES Customer (Customer_Id) on DELETE cascade on UPDATE cascade,
    Primary key(Report_Id)
);


Insert into Auth (Customer_Id,Milkman_Id,Full_Name,Email,Password,PhoneNo,SignUpDate,Milkname,Customer_Address) VALUES(NULL,'ram@gmail.com',md5('ram'),'Customer','21/11/2020');