create table Bill
(
 BillId int NOT NULL AUTO_INCREMENT,
 BillMonth text Not Null,
 TotalMoney money Not Null,
 Customer_Id Int NOT NULL,
 Milkman_Id Int NOT NULL,
 
 Foreign key (Customer_Id) REFERENCES Customer (Customer_Id) on DELETE cascade on UPDATE cascade,
 Foreign key (Milkman_Id) REFERENCES Milkman (Milkman_Id) on DELETE cascade on UPDATE cascade,
 Primary key(BillId)
);