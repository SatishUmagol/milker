<?php

require '../../core/auth.php';
$auth = new Auth;

if(isset($_COOKIE['login']) != NULL){
  require '../../index.php';
}

if(isset($_POST['login'])){  
  
  $Email = $_POST['Email'];  
  $Password = $_POST['Password'];  
  $login = $auth->Login($Email, $Password); 
  if($login == "Milkman"){
    header("location:../milk_man/index.php"); 
  }else if($login == "Customer"){
    header("location:../customer/index.php");
  }else if($login == false){
    echo" <script> alert('Email and Password is invalid') </script>";

  }
}   

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Milker | Login</title>
    <!-- plugins:css -->
    <link rel="stylesheet" href="../../assets/css/style.css"> <!-- End layout styles -->
    <link rel="shortcut icon" href="../../assets/images/favicon.png" />
  </head>
  <body>
    <div class="container-scroller">
      <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth">
          <div class="row flex-grow">
            <div class="col-lg-4 mx-auto">
              <div class="auth-form-light text-left p-5">
                <div class="brand-logo">
                  <img src="../../assets/images/logo.png" style="width:50px">
                </div>
                <h4>Hello! let's get started</h4>
                <h6 class="font-weight-light">Sign in to continue.</h6>
                <form class="pt-3" method="POST" action="">
                  <div class="form-group">
                    <input type="email" name="Email" class="form-control form-control-lg" id="exampleInputEmail1" placeholder="Email" required>
                  </div>
                  <div class="form-group">
                    <input type="password" name="Password" class="form-control form-control-lg" id="exampleInputPassword1" placeholder="Password" required>
                  </div>
                  <div class="mt-3">
                    <button name="login" type="submit" class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn">
                    SIGN IN</button>
                  </div>
                  <!-- <div class=" my-2 d-flex justify-content-between align-items-center">
                    <a href="#" class=" forgot-text auth-link text-black">Forgot password?</a>
                  </div>
                  -->
                  <div class="text-center mt-4 font-weight-light"> Don't have an account? 
                  <a href="register.php" class="text-primary">Create</a>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
  </body>
</html>