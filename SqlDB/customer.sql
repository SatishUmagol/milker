create table Customer
(
    Customer_Id int(10) NOT NULL AUTO_INCREMENT,
    Milkman_Id int(10),
    Full_Name varchar(30) NOT NULL,
    Email text NOT NULL,
    Password text NOT NULL,
    PhoneNo int(20) NOT NULL,
    SignUpDate text NOT NULL,
    Milkname text NOT NULL,
    Customer_Address text NOT NULL,
    Foreign key (Milkman_Id) REFERENCES Milkman (Milkman_Id) on DELETE cascade on UPDATE cascade,
    Primary key(Customer_Id)
);


Insert into Customer VALUES(NULL,1,'ramdev pawar','ram@gmail.com',md5('ram'),1234567890,'21/11/2020','raj,chitale,chtale cow','kothrud depo pune 39 411038');


-- Insert into Customer VALUES(NULL,1,'ramdev pawar','ram@gmail.com',md5('ram'),1234567890,'21/11/2020','raj,chitale,chtale cow','kothrud depo pune 39 411038');